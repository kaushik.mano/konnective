import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Login from "./components/login";
import { HashRouter, Switch, Route } from "react-router-dom";
import "antd/dist/antd.css";
import Registration from "./components/register";
import DashBoard from "./components/dashboard";
function App() {
  return (
    <HashRouter>
      <Switch>
        <Route exact path="/">
          <Login />
        </Route>
        <Route path="/register">
          <Registration />
        </Route>
        <Route path="/dashboard">
          <DashBoard />
        </Route>
      </Switch>
    </HashRouter>
  );
}

export default App;
