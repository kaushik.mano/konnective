import React from "react";
import { Form, Input, Typography, Button, Alert, message } from "antd";
import { EyeTwoTone, EyeInvisibleOutlined } from "@ant-design/icons";
export default function LoginForm({ setFormdat, formdat }) {
  function onChangeHandler(e) {
    setFormdat((i) => ({ ...i, [e.target.id]: e.target.value }));
  }
  const onSubmit = () => {
    console.log(formdat);
    message.success("Login is a success");
  };
  return (
    <div>
      <img src="./logo_sm.png" className="logo-top"></img>
      <div className="form-div">
        <Typography.Title>
          Sign <span style={{ color: "blue" }}>In</span>
        </Typography.Title>
        <Form onChange={onChangeHandler} onFinish={onSubmit}>
          Email address
          <Form.Item
            name="email"
            rules={[{ min: 6, message: "Please enter Email" }]}
          >
            <Input type="email" />
          </Form.Item>
          Password
          <Form.Item
            name="password"
            rules={[{ min: 6, message: "Enter a valid password" }]}
          >
            <Input.Password
              type="password"
              iconRender={(visible) =>
                visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
              }
            />
          </Form.Item>
          <a>Forgot password?</a>
          <br />
          <br />
          <Button
            htmlType="submit"
            type="primary"
            shape="round"
            className="login-button"
          >
            Sign In
            {/* backgroundImage: linear-gradient(rgb(142, 225, 253),rgb(5, 148, 224)) */}
          </Button>
          <p>
            <br />
            First time Here?<a>Click Here to Sign Up</a>
          </p>
        </Form>
      </div>
    </div>
  );
}
