import React, { useState, useEffect } from "react";
import { Layout } from "antd";
import LoginForm from "./form";
import "./loginstyle.css";
const { Sider, Content } = Layout;

export default function Login() {
  const [formdat, setFormdat] = useState({});

  //   useEffect(() => {}, [formdat]);
  return (
    <div>
      {/* <Sider width="70%" style={{ height: "100vh" }}> */}
      <img src="./welcome.png" className="login-background" />
      {/* </Sider> */}
      <LoginForm setFormdat={setFormdat} formdat={formdat} />
    </div>
  );
}
