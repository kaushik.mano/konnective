import React, { useState } from "react";
import { Col, Row, Form, Radio, Input, Typography, Button } from "antd";
export default function RegisterForm() {
  return (
    // total span is 24
    <Col xl={8} sm={24} className="register-form">
      <Row>
        <Col span={16}></Col>
        <Col span={8}>
          <img src="../logo_sm.png" className="top-logo" />
        </Col>
      </Row>
      <Row justify="left">
        <Typography.Title level={1}>
          Register <span className="red-colr">Here</span>
        </Typography.Title>
      </Row>
      <Form>
        <Row justify="left">
          <Form.Item>
            <Radio.Group>
              <Radio value="affiliate" className="radio-item">
                Affiliate
              </Radio>
              <Radio value="merchant">Merchant</Radio>
            </Radio.Group>
          </Form.Item>
        </Row>
        <Row justify="left">
          <Col span={24}>
            <Form.Item>
              {/* Email address  - text box*/}
              <label> Email Address</label>
              <Input type="email"></Input>
            </Form.Item>
          </Col>
        </Row>
        <Row justify="space-between">
          <Col span={11}>
            <Form.Item>
              {/* firstname + Last name - text box */}

              <label>First name</label>
              <Input type="text"></Input>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item>
              <label>Last name</label>
              <Input type="text"></Input>
            </Form.Item>
          </Col>
        </Row>
        <Row justify="left">
          <Col span={24}>
            <Form.Item
              rules={[
                {
                  type: Number,
                  min: 10,
                  max: 15,
                  message: "Please enter a valid number",
                },
              ]}
            >
              {/* Phone number - text box */}
              <label>Phone Number</label>
              <Input type="tel" />
            </Form.Item>
          </Col>
        </Row>
        <Row justify="left">
          <p>
            {" "}
            Tell Us About Yourself: <span className="red-colr">(Optional)</span>
          </p>
        </Row>
        <Row justify="space-between">
          {/* Business Name, URL - text box */}
          <Col span={11}>
            <Form.Item>
              <label>Business Name</label>
              <Input type="text" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item>
              <label>URL</label>
              <Input type="url" />
            </Form.Item>
          </Col>
        </Row>
        <Row justify="left">
          {/* Request Access button */}
          <Button type="default" shape="round">
            Request Access
          </Button>
        </Row>
        <Row justify="center">
          <p>
            By logging in You agree to the{" "}
            <a className="link-red"> terms of services</a>
          </p>
        </Row>
      </Form>
    </Col>
  );
}
