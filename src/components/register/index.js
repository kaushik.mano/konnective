import React from "react";
import "./registerStyle.css";
import { Row, Col } from "antd";
import RegisterForm from "./form";
export default function Registration() {
  return (
    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
      {/* for left side image */}
      <Col xl={16} sm={24}>
        <img src="./register.png" className="left-image" />
      </Col>
      {/* for right side form */}
      <RegisterForm />
    </Row>
  );
}
