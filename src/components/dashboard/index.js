import React from "react";
import { Row, Col, Layout, Breadcrumb, Avatar } from "antd";
import DashSider from "./Sider";

import "./dashStyle.scss";
import { UserOutlined } from "@ant-design/icons";
import Addnewbank from "./addnewbank";
import InactiveAccouts from "./inactiveacc.js";
const { Content, Header } = Layout;
export default function DashBoard() {
  return (
    <Layout className="dashboard-style">
      <Row>
        <Col>
          {/* left sider - collabsable*/}
          <DashSider />
        </Col>
        <Col>
          <Row style={{ maxWidth: "86vw" }}>
            <Row className="main-section1">
              <Col span={20}>
                <Breadcrumb separator="/">
                  <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
                  <Breadcrumb.Item>Bank Account Information</Breadcrumb.Item>
                </Breadcrumb>
              </Col>
              <Col span={3} className="profile">
                <Avatar icon={<UserOutlined />} />
                Profile image
              </Col>
            </Row>
            <Row className="main-section2">
              {/* add account */}
              <Addnewbank />
              {/* all bank accounts holded */}
              <InactiveAccouts />
            </Row>
            {/* breadcrumbs
             */}
          </Row>
        </Col>
      </Row>
    </Layout>
  );
}
