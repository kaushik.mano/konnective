import React, { useState, useEffect } from "react";
import { Col, Row, Layout, Menu } from "antd";
import { LeftOutlined, RightOutlined } from "@ant-design/icons";
import { images } from "./../../../assets/img/index";

const { Sider } = Layout;
export default function DashSider() {
  const [coll, setColl] = useState(true);
  const [colSty, setColsty] = useState({ marginLeft: "93%" });
  function change(e) {
    setColl((i) => !i);
    !coll
      ? setColsty({ marginLeft: "100%" })
      : setColsty({ marginLeft: "93%" });
  }
  return (
    <Sider collapsed={!coll} className="dash-sider">
      <Row>
        {/* logo */}
        <img src={images.logo_sm} />
      </Row>
      <Row onClick={() => change()} className="collapse-button" style={colSty}>
        {coll ? <LeftOutlined /> : <RightOutlined />}
      </Row>
      <Menu
        theme="dark"
        style={{
          background: "rgb(82, 119, 221)",
          borderRight: "1px block rgb(82, 119, 221)",
        }}
      >
        <Menu.Item>
          <img src={images.dashboardicon} /> Dashboard
        </Menu.Item>
        {/* icon dashboad */}

        {/* profile */}
        <Menu.Item>
          <img src={images.user} /> Profile
        </Menu.Item>
      </Menu>
    </Sider>
  );
}
