import React from "react";
import { Col, Typography, Row, Menu, Collapse, Button } from "antd";
const { Panel } = Collapse;
export default function InactiveAccouts() {
  const accounts = [
    {
      bank: "Bank of America",
      accNick: "Michael",
      routing: "1432345443244322",
      acc: "2433323222222432",
      type: "checking",
    },
    {
      bank: "JP Morgan Chase",
      accNick: "James",
      routing: "9465312547896543",
      acc: "4563179835127643",
      type: "savings",
    },
    {
      bank: "Axis",
      accNick: "Muthu",
      routing: "7986435186524865",
      acc: "7985798654286354",
      type: "savings",
    },
    {
      bank: "Axis",
      accNick: "Muthu",
      routing: "7986435186524865",
      acc: "7985798654286354",
      type: "savings",
    },
    {
      bank: "Axis",
      accNick: "Muthu",
      routing: "7986435186524865",
      acc: "7985798654286354",
      type: "savings",
    },
    {
      bank: "Axis",
      accNick: "Muthu",
      routing: "7986435186524865",
      acc: "7985798654286354",
      type: "savings",
    },
  ];
  return (
    <Col span={12}>
      <Row>
        <Typography.Title level={3}>Inactive Accounts</Typography.Title>
      </Row>
      <Row>
        <Col lg={20} sm={24}>
          <Collapse accordion>
            {accounts.map((f, i) => {
              return (
                <Panel header={`${f.bank} ${f.acc}`} key={i}>
                  <p>Bank Name: {f.bank}</p>

                  <p>Acc Nick Name: {f.accNick}</p>
                  <p>Routing Number: {f.routing}</p>
                  <p>Acc. Number: {f.acc}</p>
                  <p>Acc. Type: {f.type}</p>

                  <Button type="primary">Activate</Button>
                </Panel>
              );
            })}
          </Collapse>
        </Col>
      </Row>
    </Col>
  );
}
