import React from "react";
import { Typography, Col, Row, Input, Radio, Button } from "antd";
import Form from "antd/lib/form/Form";
import { EyeTwoTone, EyeInvisibleOutlined } from "@ant-design/icons";
export default function Addnewbank() {
  return (
    <Col lg={12} sm={24}>
      <Col span={18}>
        <Row>
          <Typography.Title level={3}>Add New Bank Details</Typography.Title>
        </Row>
        <Form>
          <Row>
            Bank Name
            <Input />
          </Row>
          <Row>
            Acc Nick Name
            <Input />
          </Row>
          <Row>
            Routing No.
            <Input.Password
              iconRender={(visible) =>
                visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
              }
            ></Input.Password>
          </Row>
          <Row>
            Confirm Routing Number
            <Input.Password
              iconRender={(visible) =>
                visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
              }
            />
          </Row>
          <Row>
            Account Number
            <Input.Password
              iconRender={(visible) =>
                visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
              }
            />
          </Row>
          <Row>
            Confirm Account Number
            <Input.Password
              iconRender={(visible) =>
                visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
              }
            />
          </Row>
          <Row>
            Accout Type:{" "}
            <Radio.Group>
              <Radio value="checking">Checking</Radio>
              <Radio value="savings">Savings</Radio>
            </Radio.Group>
          </Row>
          <Row justify="space-around">
            <Button type="default">Cancel</Button>
            <Button type="primary">Add Bank</Button>
          </Row>
        </Form>
      </Col>
    </Col>
  );
}
